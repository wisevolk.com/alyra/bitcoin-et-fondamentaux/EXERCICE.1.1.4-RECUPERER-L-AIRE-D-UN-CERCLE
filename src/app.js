class Cercle {
    constructor(rayon) {
        this.rayon = rayon;
    }

    aire() {
        return Math.PI * Math.pow(this.rayon, 2);
    }

    perimetre() {
        return Math.PI * 2 * this.rayon;
    }
}

