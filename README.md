## Exercice 1.1.4 : Récupérer l'aire et le périmètre d'un cercle (JavaScript)
Exercice 1.1.4

Construire une classe Cercle :

Son constructeur prend en paramètre un rayon.
Ses méthodes aire() et perimetre() retournent les valeurs correspondantes.

Discutons-en ensemble sur le forum :
[https://forum.alyra.fr/t/exercice-1-1-4-recuperer-laire-et-le-perimetre-dun-cercle/70](https://forum.alyra.fr/t/exercice-1-1-4-recuperer-laire-et-le-perimetre-dun-cercle/70)
